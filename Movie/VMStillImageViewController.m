//
//  VMStillImageViewController.m
//  Movie
//
//  Created by Vincent on 7/31/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMStillImageViewController.h"
#import "VMTimeLineEditBox.h"
#import "VMTimeLine.h"
#import "VMMediaLayer.h"
#import "VMMediaSegment.h"

@interface VMStillImageViewController ()

//@property (nonatomic, assign) VMTimeLine *timeLine;
@property (nonatomic, assign) VMTimeLineEditBox *timelineEditBox;

@end

@implementation VMStillImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _timelineEditBox = [[VMTimeLineEditBox alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_timelineEditBox];
    [_timelineEditBox release];
//    
//    VMMediaLayer *layer = [[VMMediaLayer alloc] init];
//    [_timeLine addMediaLayer:layer];
//    [layer release];
//
//    layer = [[VMMediaLayer alloc] init];
//    [_timeLine addMediaLayer:layer];
//    [layer release];
//    
//        
//    NSURL *url = [[NSBundle mainBundle] URLForResource:@"miku" withExtension:@"mp4"];
//    AVAsset *as = [AVAsset assetWithURL:url];
//    VMMediaSegment *segment = [[VMMediaSegment alloc] initWithAsset:as];
//    [layer addMediaSegment:segment];
//    [segment release];
//    segment.positionSecond = 0.0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

#pragma mark -

- (void)parseAsset
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"miku" withExtension:@"mp4"];
    AVAsset *as = [AVAsset assetWithURL:url];
    NSLog(@"%@", as.tracks);
    AVAssetTrack *track_a = [[as tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    NSLog(@"audio track: %@", track_a.segments);
    CMTimeRange r = track_a.timeRange;
//    AVComposition *compostion = [[AVComposition alloc] init]
}

@end
