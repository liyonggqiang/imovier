//
//  main.m
//  Movie
//
//  Created by Vincent on 7/31/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VMAppDelegate class]));
    }
}
