//
//  VMRotateViewController.m
//  Movie
//
//  Created by Vincent on 9/12/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMRotateViewController.h"

@interface VMRotateViewController ()

@property (nonatomic, retain) IBOutlet UILabel *lbTest;

@end

@implementation VMRotateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%f %f", self.view.bounds.size.width, self.view.bounds.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog(@"%s", __FUNCTION__);
    
    if (UIInterfaceOrientationPortrait == interfaceOrientation)
    {
        _lbTest.alpha = 1.0;
    }
    else if (UIInterfaceOrientationLandscapeRight == interfaceOrientation)
    {
        _lbTest.alpha = 0.0;
    }
}

@end
