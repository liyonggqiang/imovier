//
//  VMAppDelegate.h
//  Movie
//
//  Created by Vincent on 7/31/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMViewController;

@interface VMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) VMViewController *viewController;

@end
