//
//  VMTimeRuler.h
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMTimeRuler : UIView

@property (nonatomic, assign) NSUInteger ruleScale;
@property (nonatomic, readonly) NSUInteger ruleStep;
@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, readonly) NSArray *timePoints;

@property (nonatomic, retain) CALayer *tickMark;

@end
