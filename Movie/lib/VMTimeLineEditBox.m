//
//  VMTimeLineEditBox.m
//  Movie
//
//  Created by Vincent on 9/16/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMTimeLineEditBox.h"
#import "VMTimeLine.h"
#import "VMTimeRuler.h"
#import "VMTimeRuleGesture.h"
#import "VMMediaLayer.h"
#import "VMMediaSegment.h"

@interface VMTimeLineEditBox ()

@property (nonatomic, assign) UIScrollView *timeRulerBox;
@property (nonatomic, assign) UIScrollView *leftBar;

@end

@implementation VMTimeLineEditBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.autoresizingMask = 0x1f;
        
            //timeline
        _timeline = [[VMTimeLine alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _timeline.delegate = self;
        _timeline.contentInset = UIEdgeInsetsMake(40, 50, 0, 0);
        _timeline.backgroundColor = [UIColor blackColor];
        _timeline.timeRulerScale = 2;
        _timeline.duration = 200;
        [self addSubview:_timeline];
        [_timeline release];
        
            //timeline ruler
        _timeRulerBox = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 40)];
        _timeRulerBox.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//        _timeRulerBox.layer.shadowColor = [UIColor redColor].CGColor;
//        _timeRulerBox.layer.shadowOpacity = 1.0;
//        _timeRulerBox.layer.shadowRadius = 2.8;
//        _timeRulerBox.layer.shadowOffset = CGSizeMake(0, 2);
        _timeRulerBox.showsHorizontalScrollIndicator = NO;
        _timeRulerBox.showsVerticalScrollIndicator = NO;
        _timeRulerBox.bounces = NO;
        _timeRulerBox.contentInset = UIEdgeInsetsMake(0, 50, 0, 0);
        [self addSubview:_timeRulerBox];
        [_timeRulerBox release];
        
        for (UIGestureRecognizer *g in _timeRulerBox.gestureRecognizers)
        {
            [_timeRulerBox removeGestureRecognizer:g];
        }
        
        VMTimeRuleGesture *g = [[VMTimeRuleGesture alloc] init];
        g.actDelegate = self;
        [_timeRulerBox addGestureRecognizer:g];
        [g release];
        
        
        _timeRuler = [[VMTimeRuler alloc] init];
        _timeRuler.ruleScale = 2;
        _timeRuler.duration = 200;
        _timeRuler.frame = CGRectMake(0, 0, _timeline.contentSize.width, 40);
        _timeRulerBox.contentSize = _timeRuler.frame.size;
        [_timeRulerBox addSubview:_timeRuler];
        [_timeRuler release];
        
        _timeline.timeRuler = _timeRuler;
        
            //left box
        _leftBar = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 50, frame.size.height)];
        _leftBar.backgroundColor = [UIColor redColor];
        [self addSubview:_leftBar];
        [_leftBar release];
        
            //test
        VMMediaLayer *layer = [[VMMediaLayer alloc] init];
        [_timeline addMediaLayer:layer];
        [layer release];
        
        layer = [[VMMediaLayer alloc] init];
        [_timeline addMediaLayer:layer];
        [layer release];
        
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"miku" withExtension:@"mp4"];
        AVAsset *as = [AVAsset assetWithURL:url];
        VMMediaSegment *segment = [[VMMediaSegment alloc] initWithAsset:as];
        [layer addMediaSegment:segment];
        [segment release];
        segment.positionSecond = 0.0;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - uiscrollview delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    static CGPoint p;
    p.x = scrollView.contentOffset.x;
    _timeRulerBox.contentOffset = p;
}

#pragma mark - time rule gesture delegate

- (void)onTouchesBeganOnRuler:(VMTimeRuleGesture *)gesture
{
    static CGPoint p;
    p = CGPointMake(gesture.x, _timeline.tickMark.position.y);
    
    [CATransaction setAnimationDuration:0.0];
    _timeline.tickMark.position = p;
    
    p.y = 20.0;
    _timeRuler.tickMark.position = p;
}

- (void)onTouchesMovedOnRuler:(VMTimeRuleGesture *)gesture
{
    static CGPoint p;
    p = CGPointMake(gesture.x, _timeline.tickMark.position.y);
    
    [CATransaction setAnimationDuration:0.0];
    _timeline.tickMark.position = p;
    
    p.y = 20.0;
    _timeRuler.tickMark.position = p;
}

- (void)onTouchesEndOnRuler:(VMTimeRuleGesture *)gesture
{
    
}

@end
