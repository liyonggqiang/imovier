//
//  VMThumbLayer.h
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface VMThumbLayer : CALayer

- (void)makeThumbWithAsset:(AVAsset *)asset atTimes:(NSArray *)times;

@end
