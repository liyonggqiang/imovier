//
//  VMTimeRuleGesture.h
//  Movie
//
//  Created by Vincent on 9/16/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMTimeRuleGesture : UIGestureRecognizer

@property (nonatomic, assign) id actDelegate;
@property (nonatomic, assign) CGFloat x;

@end

@protocol VMTimeRuleGestureDelegate <NSObject>

- (void)onTouchesBeganOnRuler:(VMTimeRuleGesture *)gesture;

- (void)onTouchesMovedOnRuler:(VMTimeRuleGesture *)gesture;

- (void)onTouchesEndOnRuler:(VMTimeRuleGesture *)gesture;

@end