//
//  VMTimeLine.h
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMTimeRuler.h"
#import "VMMediaLayer.h"
#import "VMMediaSegment.h"

@class VMTimeRuler;
@class VMMediaLayer;

@interface VMTimeLine : UIScrollView

@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, assign) NSUInteger timeRulerScale;
@property (nonatomic, assign) VMTimeRuler *timeRuler;
@property (nonatomic, retain) NSMutableArray *mediaLayers;
@property (nonatomic, retain) CALayer *tickMark;

- (void)addMediaLayer:(VMMediaLayer *)layer;

@end
