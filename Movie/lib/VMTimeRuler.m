//
//  VMTimeRuler.m
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMTimeRuler.h"
#import "VMTimeRuleGesture.h"

@implementation VMTimeRuler

- (id)init
{
    self = [super init];
    if (self)
    {
        self.ruleScale = 1.0;
        
        self.tickMark = [CALayer layer];
        _tickMark.frame = CGRectMake(0, 0, 1.0, 40.0);
        _tickMark.backgroundColor = [UIColor yellowColor].CGColor;
        _tickMark.shadowColor = [UIColor blackColor].CGColor;
        _tickMark.shadowOpacity = 1.0;
        _tickMark.shadowRadius = 0.8;
        [self.layer addSublayer:_tickMark];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGFloat x = 0.0;
    CGFloat y = 10.0;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 1.0);
    
    CGContextSelectFont(ctx, "Helvetica", 13.0, kCGEncodingMacRoman);
    
    NSString *time_str = nil;
    
    for (int i=0; i<_duration; i++)
    {
        CGContextMoveToPoint(ctx, x, y);
        
        if (1 == _ruleScale)
        {
            CGContextAddLineToPoint(ctx, x, y+20.0);
            
            time_str = [self secondsToTime:i];
            
            CGContextSaveGState(ctx);
            CGContextScaleCTM(ctx, 1.0, -1.0);
            CGContextTranslateCTM(ctx, 0.0, -2*(y+10.0));
            CGContextShowTextAtPoint(ctx, x+3.0, y, time_str.UTF8String, strlen(time_str.UTF8String));
            CGContextRestoreGState(ctx);
            
            CGContextStrokePath(ctx);
            
            x+=100.0;
            
            continue;
        }
        
        if (0 == i%5)
        {
            CGContextAddLineToPoint(ctx, x, y+20.0);
            
            time_str = [self secondsToTime:(int)(i/5)*_ruleStep];
            
            CGContextSaveGState(ctx);
            CGContextScaleCTM(ctx, 1.0, -1.0);
            CGContextTranslateCTM(ctx, 0.0, -2*(y+10.0));
            CGContextShowTextAtPoint(ctx, x+3.0, y, time_str.UTF8String, strlen(time_str.UTF8String));
            CGContextRestoreGState(ctx);
        }
        else
        {
            CGContextAddLineToPoint(ctx, x, y+5.0);
        }
        
        CGContextStrokePath(ctx);
        
        x+=20.0;
    }
    
    
    CGContextRestoreGState(ctx);
}

- (void)setRuleScale:(NSUInteger)ruleScale
{
    _ruleScale = ruleScale;
    
    if (1 == _ruleScale)
    {
        _ruleStep = 1;
    }
    else
    {
        _ruleStep = _ruleScale * 5;
    }
}

- (NSArray *)timePoints
{
    NSMutableArray *arr = [NSMutableArray array];
    CMTime ct = kCMTimeZero;
    NSValue *tv = nil;
    
    for (int i=0; i<_duration; i++)
    {
        if (1 == _ruleScale)
        {
            ct = CMTimeMakeWithSeconds(i, 30);
            tv = [NSValue valueWithCMTime:ct];
            [arr addObject:tv];
            continue;
        }
        
        if (0 == i%_ruleStep)
        {
            ct = CMTimeMakeWithSeconds(i, 30);
            tv = [NSValue valueWithCMTime:ct];
            [arr addObject:tv];
        }
    }
    
    return arr;
}

#pragma mark -

- (NSString *)secondsToTime:(NSUInteger)sec
{
    return [NSString stringWithFormat:@"%02u:%02u", sec/60, sec%60];
}


@end
