//
//  VMMediaSegment.h
//  Movie
//
//  Created by Li Steven on 13-9-14.
//  Copyright (c) 2013年 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMTimeLine;
@class VMMediaLayer;

@interface VMMediaSegment : UIView

@property (nonatomic, assign) VMTimeLine *timeline;
@property (nonatomic, assign) VMMediaLayer *mediaLayer;
@property (nonatomic, retain) AVAsset *asset;

@property (nonatomic, assign) CGFloat positionSecond;
@property (nonatomic, assign) CMTime startTime;
@property (nonatomic, assign) CMTime endTime;

- (id)initWithAsset:(AVAsset *)asset;

- (void)remakeThumb;

@end
