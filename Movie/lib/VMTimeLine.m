//
//  VMTimeLine.m
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMTimeLine.h"
#import "VMRulerLayer.h"
#import "VMTimeRuler.h"
#import "VMMediaLayer.h"
#import "VMTimeRuleGesture.h"

@interface VMTimeLine ()

@property (nonatomic, assign) VMTimeRuleGesture *timeRulerGesture;

@end

@implementation VMTimeLine

- (void)dealloc
{
    [_tickMark release];
    [_mediaLayers release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.autoresizingMask = 0x1F;
        self.bounces = NO;
        self.mediaLayers = [NSMutableArray array];
        
        self.tickMark = [CALayer layer];
        _tickMark.frame = CGRectMake(0, 0, 1.0, self.contentSize.height);
        _tickMark.backgroundColor = [UIColor yellowColor].CGColor;
        _tickMark.shadowColor = [UIColor blackColor].CGColor;
        _tickMark.shadowOpacity = 1.0;
        _tickMark.shadowRadius = 0.8;
        [self.layer addSublayer:_tickMark];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark -

- (void)setDuration:(CGFloat)duration
{
    if (_duration == duration)
    {
        return;
    }
    
    _duration = duration;
    
    if (1 == _timeRulerScale)
    {
        self.contentSize = CGSizeMake(duration/_timeRulerScale*100.0, self.bounds.size.height);
    }
    else
    {
        self.contentSize = CGSizeMake(duration/(_timeRulerScale*5)*100.0, self.bounds.size.height);
    }
    
    _tickMark.frame = CGRectMake(0, 0, 2, self.contentSize.height);
//
//    if (!_timeRuler)
//    {
//        VMTimeRuler *ruler = [[VMTimeRuler alloc] init];
//        ruler.ruleScale = _timeRulerScale;
//        ruler.duration = _duration;
//        ruler.frame = CGRectMake(0, 0, self.contentSize.width, 40);
//        [self addSubview:ruler];
//        [ruler release];
//        _timeRuler = ruler;
//        
//        UIGestureRecognizer *g = (UIGestureRecognizer *)[self.gestureRecognizers objectAtIndex:1];
//        
//        VMTimeRuleGesture *ruler_g = [[VMTimeRuleGesture alloc] init];
//        _timeRulerGesture = ruler_g;
//        ruler_g.actDelegate = self;
//        [g requireGestureRecognizerToFail:ruler_g];
//        [ruler addGestureRecognizer:ruler_g];
//        [ruler_g release];
//    }
//    else
//    {
//        _timeRuler.frame = CGRectMake(0, 0, self.contentSize.width, 40);
//        [_timeRuler setNeedsDisplay];
//    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *v = [super hitTest:point withEvent:event];
    
    if ([v isKindOfClass:[VMTimeRuler class]])
    {
        _timeRulerGesture.enabled = YES;
    }
    else
    {
        _timeRulerGesture.enabled = NO;
    }
    
    return v;
}

#pragma mark - Media Layer

- (void)addMediaLayer:(VMMediaLayer *)layer
{
    layer.timeline = self;
    layer.frame = CGRectMake(0, _mediaLayers.count*80.0, self.contentSize.width, 80.0);
    
    [_mediaLayers addObject:layer];
    [self addSubview:layer];
    
    [self redrawMediaLayerBackground];
    
    [self.layer addSublayer:_tickMark];
}

- (void)addMediaLayer:(VMMediaLayer *)layer atIndex:(NSUInteger)index
{
    
}

- (void)redrawMediaLayerBackground
{
    int i=0;
    
    for (UIView *layer in _mediaLayers)
    {
        if (0 == i%2)
        {
            layer.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            layer.backgroundColor = [UIColor grayColor];
        }
        
        i++;
    }
}

@end
