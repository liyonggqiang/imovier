//
//  VMMediaLayer.h
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMTimeLine;
@class VMMediaSegment;

@interface VMMediaLayer : UIView

@property (nonatomic, assign) VMTimeLine *timeline;
@property (nonatomic, retain) NSMutableArray *mediaSegments;

- (void)addMediaSegment:(VMMediaSegment *)segment;

@end
