//
//  VMRulerLayer.h
//  Movie
//
//  Created by Vincent on 8/1/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface VMRulerLayer : CALayer

@property (nonatomic, assign) NSUInteger ruleScale;
@property (nonatomic, readonly) NSUInteger ruleStep;

@end
