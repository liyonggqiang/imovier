//
//  VMRulerLayer.m
//  Movie
//
//  Created by Vincent on 8/1/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMRulerLayer.h"

@implementation VMRulerLayer

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _ruleScale = 2;
        self.contentsScale = 2.0;
        self.backgroundColor = [UIColor blackColor].CGColor;
    }
    
    return self;
}


- (void)drawInContext:(CGContextRef)ctx
{
    CGFloat x = 10.0;
    CGFloat y = 10.0;
    
    CGContextSaveGState(ctx);
    
    CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetStrokeColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextSetLineWidth(ctx, 1.0);
    
    CGContextSelectFont(ctx, "Helvetica", 13.0, kCGEncodingMacRoman);
    
    NSString *time_str = nil;
    
    for (int i=0; i<80; i++)
    {
        CGContextMoveToPoint(ctx, x, y);
        
        if (1 == _ruleScale)
        {
            CGContextAddLineToPoint(ctx, x, y+20.0);
            
            time_str = [self secondsToTime:i];
            
            CGContextSaveGState(ctx);
            CGContextScaleCTM(ctx, 1.0, -1.0);
            CGContextTranslateCTM(ctx, 0.0, -2*(y+10.0));
            CGContextShowTextAtPoint(ctx, x+3.0, y, time_str.UTF8String, strlen(time_str.UTF8String));
            CGContextRestoreGState(ctx);
            
            CGContextStrokePath(ctx);
            
            x+=100.0;
            
            continue;
        }
        
        if (0 == i%5)
        {
            CGContextAddLineToPoint(ctx, x, y+20.0);
            
            time_str = [self secondsToTime:i];
            
            CGContextSaveGState(ctx);
            CGContextScaleCTM(ctx, 1.0, -1.0);
            CGContextTranslateCTM(ctx, 0.0, -2*(y+10.0));
            CGContextShowTextAtPoint(ctx, x+3.0, y, time_str.UTF8String, strlen(time_str.UTF8String));
            CGContextRestoreGState(ctx);
        }
        else
        {
            CGContextAddLineToPoint(ctx, x, y+5.0);
        }
        
        CGContextStrokePath(ctx);
        
        x+=20.0;
    }
    
    
    CGContextRestoreGState(ctx);
}

- (void)setRuleScale:(NSUInteger)ruleScale
{
    _ruleScale = ruleScale;
    
//    NSUInteger arr[10] = {1, 5, }
}

- (NSString *)secondsToTime:(NSUInteger)sec
{
    return [NSString stringWithFormat:@"%02u:%02u", sec/60, sec%60];
}

@end
