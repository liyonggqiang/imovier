//
//  VMThumbLayer.m
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMThumbLayer.h"

@interface VMThumbLayer ()

@property (nonatomic, retain) UIImage *image;

@end

@implementation VMThumbLayer

- (void)makeThumbWithAsset:(AVAsset *)asset atTimes:(NSArray *)times
{
    __block int i = 0;
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 2.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
//    AVMutableVideoComposition *vc = [AVMutableVideoComposition videoComposition];
//    vc.renderSize = CGSizeMake(100.0, 80.0);
//    vc.frameDuration = CMTimeMake(1, 30);
    
    AVAssetImageGenerator *ig = [[AVAssetImageGenerator assetImageGeneratorWithAsset:asset] retain];
//    ig.videoComposition = vc;
    [ig generateCGImagesAsynchronouslyForTimes:times
                             completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error) {
                                 
                                 CGContextDrawImage(ctx, CGRectMake(i*100.0, 0, 100.0, 80.0), image);
                                 i++;

                                 if (i == times.count)
                                 {
                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                         self.image = UIGraphicsGetImageFromCurrentImageContext();
                                         UIGraphicsEndImageContext();
                                         [ig release];
                                         [self setNeedsDisplay];
                                     });
                                 }
                             }];
}

- (void)drawInContext:(CGContextRef)ctx
{
    if (!_image)
    {
        return;
    }
    
    NSLog(@"==%@", _image);
    CGContextDrawImage(ctx, self.bounds, _image.CGImage);
    self.image = nil;
}


@end
