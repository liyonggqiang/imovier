//
//  VMTimeLineEditBox.h
//  Movie
//
//  Created by Vincent on 9/16/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMTimeLine;
@class VMTimeRuler;

@interface VMTimeLineEditBox : UIView <UIScrollViewDelegate>

@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, assign) NSUInteger timeRulerScale;
@property (nonatomic, assign) VMTimeLine *timeline;
@property (nonatomic, assign) VMTimeRuler *timeRuler;

@end
