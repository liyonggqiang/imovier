//
//  VMMediaLayer.m
//  Movie
//
//  Created by Vincent on 9/13/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMMediaLayer.h"
#import "VMTimeLine.h"
#import "VMTimeRuler.h"
#import "VMMediaSegment.h"

@interface VMMediaLayer ()

@end

@implementation VMMediaLayer

- (void)dealloc
{
    [_mediaSegments release];
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.mediaSegments = [NSMutableArray array];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)addMediaSegment:(VMMediaSegment *)segment
{
    segment.timeline = _timeline;
    segment.mediaLayer = self;
    segment.frame = self.bounds;
    
    [_mediaSegments addObject:segment];
    [self addSubview:segment];
    [segment remakeThumb];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
