//
//  VMTimeRuleGesture.m
//  Movie
//
//  Created by Vincent on 9/16/13.
//  Copyright (c) 2013 VincentStation. All rights reserved.
//

#import "VMTimeRuleGesture.h"

@implementation VMTimeRuleGesture

//- (id)init
//{
//    self = [super init];
//    
//    if (self)
//    {
////        UIGestureRecognizer *g = (UIGestureRecognizer *)[self.gestureRecognizers objectAtIndex:1];
//    }
//    
//    return self;
//}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_actDelegate)
    {
        UITouch *t = [touches anyObject];
        CGPoint p = [t locationInView:t.view];
        _x = p.x;
        [_actDelegate performSelector:@selector(onTouchesBeganOnRuler:) withObject:self];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_actDelegate)
    {
        UITouch *t = [touches anyObject];
        CGPoint p = [t locationInView:t.view];
        _x = p.x;
        [_actDelegate performSelector:@selector(onTouchesMovedOnRuler:) withObject:self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_actDelegate)
    {
        [_actDelegate performSelector:@selector(onTouchesEndOnRuler:) withObject:self];
    }
}

@end
