//
//  VMMediaSegment.m
//  Movie
//
//  Created by Li Steven on 13-9-14.
//  Copyright (c) 2013年 VincentStation. All rights reserved.
//

#import "VMMediaSegment.h"
#import "VMTimeLine.h"
#import "VMTimeRuler.h"
#import "VMMediaLayer.h"
#import "VMThumbLayer.h"

@implementation VMMediaSegment

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithAsset:(AVAsset *)asset
{
    self = [super init];
    
    if (self)
    {
        self.asset = asset;
        _startTime = kCMTimeZero;
        _endTime = asset.duration;
    }
    
    return self;
}

- (void)setPositionSecond:(CGFloat)positionSecond
{
    if (positionSecond == _positionSecond)
    {
        return;
    }
    
    _positionSecond = positionSecond;
    
    CGFloat x = positionSecond * (_timeline.contentSize.width/_timeline.duration);
    
    CGRect f = self.frame;
    f.origin.x = x;
    self.frame = f;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)remakeThumb
{
    Float64 duration = CMTimeGetSeconds(CMTimeSubtract(_endTime, _startTime));
    
    if (duration > _timeline.duration)
    {
        duration = _timeline.duration;
    }
    
    CGFloat w = duration * (_timeline.contentSize.width/_timeline.duration);
    int points = ceil(duration/_timeline.timeRuler.ruleStep);
    
    NSArray *time_points = [_timeline.timeRuler.timePoints subarrayWithRange:NSMakeRange(0, points)];
    
    VMThumbLayer *thumb_layer = [[VMThumbLayer alloc] init];
    thumb_layer.frame = CGRectMake(0, 0, w, self.bounds.size.height);
    [self.layer addSublayer:thumb_layer];
    [thumb_layer release];
    [thumb_layer makeThumbWithAsset:_asset atTimes:time_points];
    
    CGRect f = self.frame;
    f.size.width = w;
    self.frame = f;
}

@end
